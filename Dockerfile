FROM python:3
ENV PYTHONUNBUFFERED=1
WORKDIR /Blog
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY ./config/nginx/django.conf /etc/nginx/nginx.conf
COPY . .

